# ArgoCD



# ArgoCD install

참고: https://argo-cd.readthedocs.io/en/stable/getting_started/

# 1. Install Argo CD
```sh
$ kubectl create namespace argocd

$ kubectl apply -n argocd -f \
    https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

$ alias ka='kubectl -n argocd'

$ ka get pod -w


$ ka get svc

NAME                                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
argocd-applicationset-controller          ClusterIP   10.43.53.247    <none>        7000/TCP                     34s
argocd-dex-server                         ClusterIP   10.43.252.22    <none>        5556/TCP,5557/TCP,5558/TCP   34s
argocd-metrics                            ClusterIP   10.43.24.96     <none>        8082/TCP                     34s
argocd-notifications-controller-metrics   ClusterIP   10.43.177.0     <none>        9001/TCP                     34s
argocd-redis                              ClusterIP   10.43.232.230   <none>        6379/TCP                     34s
argocd-repo-server                        ClusterIP   10.43.153.196   <none>        8081/TCP,8084/TCP            34s
argocd-server                             ClusterIP   10.43.161.93    <none>        80/TCP,443/TCP               34s
argocd-server-metrics                     ClusterIP   10.43.247.207   <none>        8083/TCP  


# clean up


$ kubectl -n argocd delete  -f \
    https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

```


# 2. Argo CD API Server에 접근
## 1) 서비스 expose
기본적으로 Argo CD API 서버는 외부 IP로 노출되지 않는다.  API 서버에 액세스하려면 다음 중 하나를 선택하여 Argo CD API 서버를 노출시킨다.

```
1) Service Type Load Balancer
2) Ingress
3) Port Forwarding
4) node port
```


### (1) ingress


```sh
$ cat > 15.argocd-ingress.yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: argocd-ingress
  annotations:
    kubernetes.io/ingress.class: "traefik"
spec:
  rules:
  - host: "argocd.song01.ktcloud.com"
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: argocd-server
            port:
              number: 80
—


$ ka apply -f 15.argocd-ingress.yaml

curl localhost:30908/ -H "Host:argocd.song01.ktcloud.com"
```



### (2) node port 로 테스트

service 를 ClusterIP type 에서 NodePort type 으로 변경
```sh
$ ka edit svc argocd-server

…
spec:
  clusterIP: 10.43.190.223
  clusterIPs:
  - 10.43.190.223
  externalTrafficPolicy: Cluster
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: http
    nodePort: 31284
    port: 80
    protocol: TCP
    targetPort: 8080
  - name: https
    nodePort: 31762
    port: 443
    protocol: TCP
    targetPort: 8080
  selector:
    app.kubernetes.io/name: argocd-server
  sessionAffinity: None
  type: ClusterIP          <- NodePort 로 변경


ka get svc
NAME                                      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
argocd-applicationset-controller          ClusterIP   10.43.53.247    <none>        7000/TCP                     2m15s
argocd-dex-server                         ClusterIP   10.43.252.22    <none>        5556/TCP,5557/TCP,5558/TCP   2m15s
argocd-metrics                            ClusterIP   10.43.24.96     <none>        8082/TCP                     2m15s
argocd-notifications-controller-metrics   ClusterIP   10.43.177.0     <none>        9001/TCP                     2m15s
argocd-redis                              ClusterIP   10.43.232.230   <none>        6379/TCP                     2m15s
argocd-repo-server                        ClusterIP   10.43.153.196   <none>        8081/TCP,8084/TCP            2m15s
argocd-server-metrics                     ClusterIP   10.43.247.207   <none>        8083/TCP                     2m15s
argocd-server                             NodePort    10.43.161.93    <none>        80:31501/TCP,443:32437/TCP   2m15s
```






## 2) argocd-ui 접근
http://211.254.212.105:31501

![img](https://lh4.googleusercontent.com/L-VJEIIEW-d_khi-i5VpMmZQl2rfonUnRqwuMcH8NwzD4-pLjj4jq9bj2OoN5UW_F4GOAyUYgKf3f2W0GSR91sTzQUf2iB2cGBIsUmDAb1ZE55eKSVttC0nwPloMlO-hHtcDldSjPR_-5JrRoOaLyA)






## 3) Password 확인
Argo CD는 최초 admin account의 초기 password를 kubernetes 의 secret 으로 저장해 놓는다. 아래와 같이 password를 얻는다.
```sh
# 1) jsonpath 방식 

$ ka get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
s9nWCHYUt6VpjUkQ


# 2) yaml 방식
$ ka -n argocd get secret argocd-initial-admin-secret -o yaml

apiVersion: v1
data:
  password: d3ZJeVBjc3BWY1lQdzFzZQ==
kind: Secret
metadata:
  creationTimestamp: "2022-05-21T11:16:08Z"
  name: argocd-initial-admin-secret
  namespace: argocd
  resourceVersion: "2280"
  uid: 683ede7d-05db-44fe-9e3c-ebbd6eb18d0b
type: Opaque


$ echo d3ZJeVBjc3BWY1lQdzFzZQ== | base64 --decode
s9nWCHYUt6VpjUkQ --> 차후 argo1234!  로 변경
```


# 3. Creating Apps Via UI
```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: guestbook
spec:
  destination:
    name: ''
    namespace: song
    server: 'https://kubernetes.default.svc'
  source:
    path: guestbook
    repoURL: 'https://github.com/argoproj/argocd-example-apps.git'
    targetRevision: HEAD
    directory:
      jsonnet:
        tlas: []
      recurse: false
  project: default
---
```


# 4. Creating Apps Via CLI 
## 1) Download CLI

Argo CD CLI 설치 한다.

최종버젼 release : https://github.com/argoproj/argo-cd/releases/latest
```sh
$ wget https://github.com/argoproj/argo-cd/releases/download/v2.3.4/argocd-linux-amd64

  chmod +x argocd-linux-amd64
  mv ./argocd-linux-amd64 /usr/local/bin/argocd
```


## 2) ArgoCD Login
```sh
$ argocd login localhost:30374


$ argocd login argocd-argocd.apps.211-34-231-82.nip.io \
    --insecure \
    --username admin \
    --password New1234!

'admin:login' logged in successfully
Context 'argocd-argocd.apps.211-34-231-82.nip.io' updated




$ argocd cluster list
SERVER                          NAME        VERSION  STATUS      MESSAGE  PROJECT
https://kubernetes.default.svc  in-cluster  1.20+    Successful

$ argocd cluster list
SERVER                          NAME        VERSION  STATUS      MESSAGE  PROJECT
https://kubernetes.default.svc  in-cluster  1.20+    Successful
root@songgram:~/song/del/argocd-helm-kubernetes/helm/devopsodia# argocd app list
NAME                            CLUSTER                         NAMESPACE  PROJECT  STATUS     HEALTH   SYNCPOLICY  CONDITIONS  REPO                                    PATH             TARGET
argocd/edu13-gitops             https://kubernetes.default.svc  edu30      default  Synced     Healthy  Auto        <none>      https://github.com/shclub/edu13-gitops  .                HEAD
argocd/edu31-apps               https://kubernetes.default.svc  edu31      edu31    Synced     Healthy  Auto-Prune  <none>      https://github.com/shclub/edu14-2.git   apps             HEAD
argocd/edu31-frontend           https://kubernetes.default.svc  edu31      edu31    Synced     Healthy  Auto-Prune  <none>      https://github.com/shclub/edu14-2.git   charts/frontend  HEAD
argocd/edu31-helm-db            https://kubernetes.default.svc  edu31      edu31    OutOfSync  Healthy  Auto-Prune  <none>      https://github.com/shclub/edu14-2.git   charts/helm-db   HEAD
argocd/edu31-kustomize-backend  https://kubernetes.default.svc  edu31      edu31    Synced     Healthy  Auto-Prune  <none>      https://github.com/shclub/edu14-2.git   charts/backend   HEAD


```





## 3) Creating Apps
#### guestbook

```sh
$ argocd app create guestbook \
    --project default \
    --repo https://github.com/argoproj/argocd-example-apps.git \
    --path guestbook \
    --dest-server https://kubernetes.default.svc \
    --dest-namespace song


FATA[0029] rpc error: code = InvalidArgument desc = application spec for guestbook is invalid: 
InvalidSpecError: Unable to get app details: rpc error: code = Unknown 
desc = Get "https://github.com/argoproj/argocd-example-apps.git/info/refs?service=git-upload-pack": 
context deadline exceeded (Client.Timeout exceeded while awaiting headers)

→ 안된다.  인증서때문인가???
→ dns 때문이다.  
→ coreDns 가 뭔가 잘 작동하지 않는다.
→ workaround 로 /etc/resolve.conf 를 조정하면 되긴하는데… 쩝~~~



$ argocd app sync guestbook
```

#### userlist

```sh
$ argocd app create userlist \
    --project song \
    --repo https://github.com/ssongman/UserList.git \
    --revision test \
    --path k8s_istio_yaml/k8s \
    --dest-server https://kubernetes.default.svc \
    --dest-namespace song

$ argocd app sync userlist   <-- deploy

$ argocd app list

$ argocd app delete userlist
```



#### devopsodia-helm

```sh
# Create a Helm app from a Helm repo
$ argocd app create devopsodia-helm \
     --project song \
     --repo http://nexus347-nexus-repository-manager.song.svc:8081/repository/argocd-helm-repo/ \
     --helm-chart devopsodia \
     --revision 1 \
     --dest-namespace song \
     --dest-server https://kubernetes.default.svc



$ argocd app sync devopsodia-helm  <-- deploy

$ argocd app list

$ argocd app delete devopsodia-helm
```




## 4) Sync (Deploy) The Application
```sh
$ argocd app sync guestbook
```






# 5. Account 관리
## 1) Password 변경

```sh
$ argocd account update-password
*** Enter password of currently logged in user (admin):
*** Enter new password for user admin:
*** Confirm new password for user admin:

```






## 2) 계정추가

```sh
$ kubectl -n argocd get configmap argocd-cm -o yaml

—
data:
  accounts.alice: apiKey,login
  accounts.alice.enabled: "false"

—


$ kubectl -n argocd apply -f argocd-cm.yml


$ argocd account list

NAME   ENABLED  CAPABILITIES
admin  true     login
alice  false    apiKey, login



# password 변경
# 양식
argocd account update-password --account <new-username> --new-password <new-password>


$ argocd account update-password --account alice  --new-password alice1234!

```







## 3) RBAC for Local User

```sh
$ kubectl -n argocd get configmap argocd-rbac-cm -o yaml
apiVersion: v1
kind: ConfigMap
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"ConfigMap","metadata":{"annotations":{},"labels":{"app.kubernetes.io/name":"argocd-rbac-cm","app.kubernetes.io/part-of":"argocd"},"name":"argocd-rbac-cm","namespace":"argocd"}}
  creationTimestamp: "2022-05-26T14:21:25Z"
  labels:
    app.kubernetes.io/name: argocd-rbac-cm
    app.kubernetes.io/part-of: argocd
  name: argocd-rbac-cm
  namespace: argocd
  resourceVersion: "1731"
  uid: 97b4a1cd-476d-44e1-a8c2-979cf049760f


# 아래와 같이 rbac 내용을 추가한다.

—---

data:
  policy.csv: |
    p, role:org-admin, applications, *, */*, allow
    p, role:org-admin, clusters, get, *, allow
    p, role:org-admin, repositories, get, *, allow
    p, role:org-admin, repositories, create, *, allow
    p, role:org-admin, repositories, update, *, allow
    p, role:org-admin, repositories, delete, *, allow
    g, alice, role:org-admin
  policy.default: role:''


```









## 4) password 잊었을때

참고 : https://github.com/argoproj/argo-cd/blob/master/docs/faq.md

```sh

song 계정의 password 를 등록할때



$ argocd account bcrypt --password songpass!

$2a$10$0FQOZNNUkp4WxUfhvdVD0eao8ZZDOaPGy6a7jED2IxyNDfIajv2zu

$  echo $2a$10$0FQOZNNUkp4WxUfhvdVD0eao8ZZDOaPGy6a7jED2IxyNDfIajv2zu | base64
YTAtYmFzaEZRT1pOTlVrcDRXeFVmaHZkVkQwZWFvOFpaRE9hUEd5NmE3akVEMkl4eU5EZklhanYy

아래처럼 argocd-secret 에 등록한다.

kubectl -n argocd patch secret argocd-secret \
  -p '{"stringData": {
    "accounts.song.password": "$2a$10$0FQOZNNUkp4WxUfhvdVD0eao8ZZDOaPGy6a7jED2IxyNDfIajv2zu",
    "accounts.song.passwordMtime": "'$(date +%FT%T%Z)'"
  }}'
  


```







# 6. Argo rollout

참조: https://devocean.sk.com/blog/techBoardDetail.do?ID=163189
## 1) argo rollout?
Argo rollout 은 Progressive Delivery 를 지원하는 툴로서 Deployment 와 비슷한 역할을 수행한다. 실제 Deployment 대신 Rollout 이라는 리소스가 실행되면서 pod가 실행된다.Canary 배포를 진행할 때 일시적으로 배포를 홀딩한 상태에서 new version 에 대한 배포가 성공되었는지를 Metric 으로 판단하여 안전하게 배포를 완료할 수 있다.



![img](https://lh6.googleusercontent.com/AB0203OBNhH6L9Qb6nlAHvK5LB1yx2a6A48vW-kDJJGjX9B-1tTItWrP9ixmNB0sWEacvSa0M6UnP7yHwlbTj9RvJDaS8qzJvl-2pOzXr_-5gL4nZn_HOqfN1Eh3gMuPdX0GgDNKswHqS57jk9KaYw)



## 2) Argo rollouts install

```sh

$ k create namespace argo-rollouts
$ k apply -n argo-rollouts -f https://github.com/argoproj/argo-rollouts/releases/latest/download/install.yaml


customresourcedefinition.apiextensions.k8s.io/analysisruns.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/analysistemplates.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/clusteranalysistemplates.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/experiments.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/rollouts.argoproj.io created
serviceaccount/argo-rollouts created
clusterrole.rbac.authorization.k8s.io/argo-rollouts created
clusterrole.rbac.authorization.k8s.io/argo-rollouts-aggregate-to-admin created
clusterrole.rbac.authorization.k8s.io/argo-rollouts-aggregate-to-edit created
clusterrole.rbac.authorization.k8s.io/argo-rollouts-aggregate-to-view created
clusterrolebinding.rbac.authorization.k8s.io/argo-rollouts created
secret/argo-rollouts-notification-secret created
service/argo-rollouts-metrics created
deployment.apps/argo-rollouts created

# clean up
$ k delete -n argo-rollouts -f https://github.com/argoproj/argo-rollouts/releases/latest/download/install.yaml

```



## 3) Kubectl Plugin Installation
kubectl 플러그인은 선택 사항이지만 CLI 에서 롤아웃을 관리하고 시각화하는데 매우 편리하다.

```sh

$ curl -LO https://github.com/argoproj/argo-rollouts/releases/latest/download/kubectl-argo-rollouts-darwin-amd64

$ chmod +x ./kubectl-argo-rollouts-darwin-amd64

$ sudo mv ./kubectl-argo-rollouts-darwin-amd64 /usr/local/bin/kubectl-argo-rollouts

$ kubectl argo rollouts version

—--

명령예제


  # Get guestbook rollout and watch progress
  kubectl argo rollouts get rollout guestbook -w

  # Pause the guestbook rollout
  kubectl argo rollouts pause guestbook

  # Promote the guestbook rollout
  kubectl argo rollouts promote guestbook

  # Abort the guestbook rollout
  kubectl argo rollouts abort guestbook

  # Retry the guestbook rollout
  kubectl argo rollouts retry guestbook



```



## 4) Sample rollout
kubectl 플러그인은 선택 사항이지만 CLI 에서 롤아웃을 관리하고 시각화하는데 매우 편리하다.

```sh

$ cd ~/song/argo-rollout-demo

$ curl -Lo basic-rollout-blue.yaml https://raw.githubusercontent.com/argoproj/argo-rollouts/master/docs/getting-started/basic/rollout.yaml

$ curl -Lo basic-service.yaml https://raw.githubusercontent.com/argoproj/argo-rollouts/master/docs/getting-started/basic/service.yaml

$ kubectl -n argo-rollouts apply -f basic-rollout-blue.yaml
$ kubectl -n argo-rollouts apply -f basic-service.yaml

$ kubectl -n argo-rollouts patch svc rollouts-demo --patch \
'{"spec": { "type": "NodePort", "ports": [ { "nodePort": 31080, "port": 80, "protocol": "TCP", "targetPort": "http", "name": "http" } ] } }'




# clean up
$ cd ~/song/argo-rollout-demo
$ kar delete -f basic-rollout-blue.yaml
$ kar delete -f basic-service.yaml

```



http://k2-master01:31080



![img](https://lh6.googleusercontent.com/cfLpBum6RJCc7Ys-GVB6YBBpwo1YPCdZ1_d3JTt5OEQZDCNoJAKLJjbON29UIFjsauP5EQN-0oGVk5xwETEqH903pFxA_jocc6r3Pk1YpPeO80fT0eaGBG1MWac43c-ITybnLHvBPQ4NmZEdCK5SGg)



```sh
콘솔 보기
# kubectl argo rollouts get rollout rollouts-demo --watch

```

배포상태



```sh

$ kubectl argo rollouts get rollout -n argo-rollouts rollouts-demo
Name:            rollouts-demo
Namespace:       argo-rollouts
Status:          ✔ Healthy
Strategy:        Canary
  Step:          8/8
  SetWeight:     100
  ActualWeight:  100
Images:          argoproj/rollouts-demo:blue (stable)
Replicas:
  Desired:       5
  Current:       5
  Updated:       5
  Ready:         5
  Available:     5

NAME                                       KIND        STATUS     AGE  INFO
⟳ rollouts-demo                            Rollout     ✔ Healthy  32m
└──# revision:1
   └──⧉ rollouts-demo-687d76d795           ReplicaSet  ✔ Healthy  32m  stable
      ├──□ rollouts-demo-687d76d795-9dfrs  Pod         ✔ Running  32m  ready:1/1
      ├──□ rollouts-demo-687d76d795-flvfx  Pod         ✔ Running  32m  ready:1/1
      ├──□ rollouts-demo-687d76d795-knscn  Pod         ✔ Running  32m  ready:1/1
      ├──□ rollouts-demo-687d76d795-wrwvm  Pod         ✔ Running  32m  ready:1/1
      └──□ rollouts-demo-687d76d795-z2279  Pod         ✔ Running  32m  ready:1/1




kubectl argo rollouts set image rollouts-demo rollouts-demo=argoproj/rollouts-demo:yellow -n argo-rollouts


# kubectl argo rollouts promote rollouts-demo


```





진행상태

```sh
# kubectl argo rollouts set image rollouts-demo rollouts-demo=argoproj/rollouts-demo:yellow -n argo-rollouts


# kubectl argo rollouts promote rollouts-demo

```



완료

```sh
Name:            rollouts-demo
Namespace:       argo-rollouts
Status:          ✔ Healthy
Strategy:        Canary
  Step:          8/8
  SetWeight:     100
  ActualWeight:  100
Images:          argoproj/rollouts-demo:yellow (stable)
Replicas:
  Desired:       5
  Current:       5
  Updated:       5
  Ready:         5
  Available:     5

NAME                                       KIND        STATUS        AGE  INFO
⟳ rollouts-demo                            Rollout     ✔ Healthy     81m
├──# revision:2
│  └──⧉ rollouts-demo-6cf78c66c5           ReplicaSet  ✔ Healthy     42m  stable
│     ├──□ rollouts-demo-6cf78c66c5-hnfkj  Pod         ✔ Running     42m  ready:1/1
│     ├──□ rollouts-demo-6cf78c66c5-74vrr  Pod         ✔ Running     67s  ready:1/1
│     ├──□ rollouts-demo-6cf78c66c5-6txxp  Pod         ✔ Running     56s  ready:1/1
│     ├──□ rollouts-demo-6cf78c66c5-vx7bx  Pod         ✔ Running     46s  ready:1/1
│     └──□ rollouts-demo-6cf78c66c5-n2cnf  Pod         ✔ Running     36s  ready:1/1
└──# revision:1
   └──⧉ rollouts-demo-687d76d795           ReplicaSet  • ScaledDown  81m

```





# Hands in
# 1. argocd sync

## 1) github 에서 내용 확인

### (1) github 에서 

userlist deployment replicas 2 로 수정

### (2) 상태확인

argocd outofsync 상태확인 :

### (3) sync 수행

### (4) 상태 확인

userlist RoundRobbin 상태확인 : 잘될것이다.

argocd outofsync 상태확인 : sync 로 변경됨



## 2) argocd 에서 직접 수정후 다시 sync
### (1) argocd 에서 

userlist replicas 2 로 수정

### (2) 상태 확인

userlist RoundRobbin 상태확인

argocd outofsync 상태확인

### (3) sync 버튼 클릭

### (4) 상태 확인

userlist RoundRobbin 상태확인

argocd Synced 상태확인
